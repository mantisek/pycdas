import updater
import sys


force_update = False

if __name__ == "__main__":
    n = len(sys.argv)
    #print(str(n))
    if n > 1:
        for i in range(1, n):
            #print("Option given: " + sys.argv[i])
            if i == '-f':
                print('DEBUG: Force update called')
                force_update = True
            else:
                print("TODO: HELP PAGE")

    updater.update_check(force_update)

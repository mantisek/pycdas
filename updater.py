from tqdm import tqdm
import os, sys, time, requests
from pathlib import Path

FILENAME = "testfile.bin"
MAX_AGE = 1 #How old the file can be before it's considered out of date, in days

print("update running")

def file_check(filename):
    path = Path(filename)
    current_time = time.time()


    if path.is_file():
        creation_time = os.path.getctime(filename)
        file_age = (current_time - creation_time) // (24 * 36000)
        print(filename + " exists...")
        if file_age >= MAX_AGE:
            print(filename + " is more than " + str(MAX_AGE) + " days out of date. Would you like to update? (This may take a while) (Y/n)")
            if input().upper() == "Y":
                # return True
                return True
            else:
                return False
        else:
            print(filename + " is " + str(file_age) + " days old.")
            return False
    else:
        return True

def download_file():
    #url = "https://data.seattle.gov/api/views/tazs-3rd5/rows.json"
    url = "https://sabnzbd.org/tests/internetspeed/50MB.bin"  # For testing purposes on a bad connection.

    # The header of the dl link has a Content-Length which is in bytes.
    # The bytes is in string hence has to convert to integer.
    filename = os.path.basename(url)

    chunk_size = 1024

    # Use the requests.get with stream enable, with iter_content by chunk size,
    # the contents will be written to the dl_path.
    # tqdm tracks the progress by progress.update(datasize)
    with requests.get(url, stream=True) as r, open(FILENAME, "wb") as f, tqdm(
            unit="B",  # unit string to be displayed.
            unit_scale=True,  # let tqdm to determine the scale in kilo, mega..etc.
            unit_divisor=1024,  # is used when unit_scale is true
            #total=filesize,  # the total iteration.
            file=sys.stdout,  # default goes to stderr, this is the display on console.
            desc="Downloading most up to date crime data... ( " + filename + " )"
    ) as progress:
        for chunk in r.iter_content(chunk_size=chunk_size):
            # download the file chunk by chunk
            datasize = f.write(chunk)
            # on each chunk update the progress bar.
            progress.update(datasize)

# If the conditions are met for updating, download the file
def update_check(force_update):
    print("update_check called")
    if file_check(FILENAME):
        download_file()
    elif force_update:
        print("Forcing update...")
        download_file()
    else:
        print("Skipping update.")
